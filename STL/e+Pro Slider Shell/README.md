# e+Pro Slider

The e+Pro is an adjustable variant of my original eReader shell design. It was originally intended to find the ideal focal range for the camera unit but it proved very useful in being able to focus the camera for card cases of varying thicknesses.\
\
![ePro Slider Cutaway](/images/e+Pro Slider Cutaway.PNG)
\
The e+Pro Slider can be used with all existing bottom shell parts, which can be found [here](../Bottom Shell Parts).\
**Use M1.7x5 micro screws for the four external mounting points nearest to the cartridge end. Use M1.7x6 micro screws for the two rear tray mounting points, the two slider screw points (don't overtighten!) and the two internal camera mounting points.**\
\
This shell should be printed out of a tough but not brittle material such as ABS/ASA or PETG. A 0.4mm nozzle is sufficient to retain all tolerance-critical details on this part.\
For the sliding camera carriage, it would be wise to print slow to get the best print quality. A 0.25mm nozzle is recommended for the best precision and fitment but with a well-tuned printer or adjustable line width you can get away with a 0.4mm nozzle.\
\
I recommend at the very least a 30% cubic or 20% gyroid infill and a 1.2mm wall thickness for all parts.
