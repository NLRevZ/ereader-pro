# Slope Shims
\
![PSA Shim](/images/psa_shim.PNG)

These shims are for use with the sloped tray found in the [Bottom Shell Parts](../Bottom Shell Parts) folder.\
Print both tray and shim parts flat side down, no supports needed.\
These can be any hard material such as PLA, PETG, ABS/ASA, a 0.4mm or 0.6mm nozzle is recommended to reduce print time. I recommend 25% cubic or 15% gyroid infill and at least 1.2mm wall thickness for all parts.
