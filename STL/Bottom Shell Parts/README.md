# Bottom Shell Parts


To complete the e+Pro unit, you'll need at least the Bottom Front file from this folder and a rear tray of your choosing.\
There's a flat tray onto which you can put either the [Flat Shims](../Flat Shims) or the [Hook Shims](../Hook Shims) to get your card (case) to the right height for scanning.\
Alternatively, a sloped tray is available for PSA card cases onto which you can put the [accompanying shims](../Slope Shims).\
\
A general rule of thumb for printing is to assume all of these prints are to be made using ASA (preferably) or PLA. All of these parts can be printed with a 0.4mm nozzle; make sure supports are enabled for the Bottom Front part and face it flat side down.
