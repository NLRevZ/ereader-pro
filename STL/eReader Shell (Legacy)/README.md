# eReader Shell - the original

This is the original eReader shell design, kept here for legacy purposes. It has been superseded by the [e+Pro Slider](../e+Pro Slider Shell) shell.
\
This top shell, combined with the desired [bottom shell parts](../Bottom Shell Parts), makes for a complete eReader shell into which the hardware from an original e-Reader can be mounted.\
\
**Use M1.7x5 micro screws for the four external mounting points nearest to the cartridge end. Use M1.7x6 micro screws for the two rear tray mounting points and the two internal camera mounting points.**\
\
This shell should be printed out of a tough but not brittle material such as ABS/ASA or PETG. A 0.4mm nozzle is sufficient to retain all tolerance-critical details on this part.\
For the sliding camera carriage, it would be wise to print slow to get the best print quality. A 0.25mm nozzle is recommended for the best precision and fitment but with a well-tuned printer or adjustable line width you can get away with a 0.4mm nozzle.\
\
I recommend at the very least a 30% cubic or 20% gyroid infill and a 1.2mm wall thickness for all parts.
