# Hook Shims
\
![Hook Shim](/images/hook_shim.png)

These shims are for use with the flat tray found in the [Bottom Shell Parts](../Bottom Shell Parts) folder.\
They're designed to have some friction to hold them in place when mounted; if after printing you find the shims too difficult or impossible to slide onto or off of the top shell, take a round file or some sandpaper and gently adjust the inner corners of the shims to ease the fit.\
\
Print the required tray flat side down, no supports needed. The shims are flat so top or bottom face down does not matter.\
These can be any hard material such as PLA, PETG, ABS/ASA, a 0.4mm or 0.6mm nozzle is recommended to reduce print time. I recommend 25% cubic or 15% gyroid infill and at least 1.2mm wall thickness for all parts.
