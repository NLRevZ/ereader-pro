# eReader Pro (and e+Pro)


The e+Pro is _an alternative shell_ for everyone's favorite dotcode card reader.\
Its main purpose is to provide people with a means of scanning special dot-coded cards if they reside inside protective cases that cannot be taken off without causing damage to either the card or its case, and therefore cannot fit the original eReader units' card slot.

![ePro rear](images/ePro_outer2.PNG)
![ePro front](images/ePro_outer.PNG)\
_Above: My original eReader design._
\
\
It is designed to be used with the original screws, though if your 3D printer is not the most accurate I strongly suggest you use M1.7 x 5 and M1.7 x 6 (any thread type) button head screws, the likes of which you can often find in assortment boxes on Amazon and such.

There are print notes for some of the STL files found among the project files in each relevant subfolder.

A general rule of thumb for printing is to assume all of the top shell parts are to be made using ASA (preferably), ABS or PETG, also a 0.25mm nozzle is recommended for the top shell parts due to critical alignment tolerances for some details. Other parts may be printed using a plain 0.4mm nozzle, though some details may not be as accurate. For details, reference the notes contained in the README.md file in each folder.

[![CC-BY-SA 4.0](https://licensebuttons.net/l/by-sa/3.0/88x31.png)](/LICENSE)


